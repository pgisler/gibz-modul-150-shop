<?php
namespace One50\Shop\Domain\Repository;

/*
 * This file is part of the One50.Shop package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\QueryInterface;
use Neos\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class ProductRepository extends Repository
{
	
	/**
	 * Default ordering by column "title" (ascending)
	 *
	 * @var array
	 */
	protected $defaultOrderings = array ('title' => QueryInterface::ORDER_ASCENDING);

}
