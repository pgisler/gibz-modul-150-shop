<?php
namespace One50\Shop\Domain\Repository;

/*
 * This file is part of the One50.Shop package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class UserRepository extends Repository
{

    // add customized methods here

}
