<?php
namespace One50\Shop\Domain\Model;

/*
 * This file is part of the One50.Shop package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class OrderItem implements CartOrderItemInterface {
	
	/**
	 * @var \One50\Shop\Domain\Model\Order
	 * @ORM\ManyToOne(inversedBy="items")
	 */
	protected $parentOrder;
	
	/**
	 * @var \One50\Shop\Domain\Model\Product
	 * @ORM\ManyToOne
	 */
	protected $product;
	
	/**
	 * @var integer
	 * @Flow\Validate(type="NotEmpty")
	 * @Flow\Validate(type="Integer")
	 * @Flow\Validate(type="NumberRange", options={"minimum"=1})
	 */
	protected $quantity;
	
	
	/**
	 * @return \One50\Shop\Domain\Model\Order
	 */
	public function getParentOrder() {
		return $this->parentOrder;
	}
	
	/**
	 * @param \One50\Shop\Domain\Model\Order $parentOrder
	 * @return void
	 */
	public function setParentOrder(\One50\Shop\Domain\Model\Order $parentOrder) {
		$this->parentOrder = $parentOrder;
	}
	
	/**
	 * @return \One50\Shop\Domain\Model\Product
	 */
	public function getProduct() {
		return $this->product;
	}
	
	/**
	 * @param \One50\Shop\Domain\Model\Product $product
	 * @return void
	 */
	public function setProduct(\One50\Shop\Domain\Model\Product $product) {
		$this->product = $product;
	}
	
	/**
	 * @return integer
	 */
	public function getQuantity() {
		return $this->quantity;
	}
	
	/**
	 * @param integer $quantity
	 * @return void
	 */
	public function setQuantity($quantity) {
		$this->quantity = $quantity;
	}
	
}
