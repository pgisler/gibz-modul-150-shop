<?php

namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */

use Neos\Flow\Annotations as Flow;
use One50\Shop\Domain\Model\Address;
use One50\Shop\Domain\Model\User;
use Neos\Error\Messages\Message;
use Neos\Flow\Security\Account;
use One50\Shop\Service\AccountService;

class UserController extends AbstractActionController
{

    /**
     * @Flow\Inject
     * @var \One50\Shop\I18n\Translator
     */
    protected $translator;

    /**
     * @Flow\Inject
     * @var \Neos\Flow\Security\AccountFactory
     */
    protected $accountFactory;

    /**
     * @Flow\Inject
     * @var \Neos\Flow\Security\AccountRepository
     */
    protected $accountRepository;

    /**
     * @Flow\Inject
     * @var \One50\Shop\Domain\Repository\UserRepository
     */
    protected $userRepository;

    /**
     * @Flow\Inject
     * @var AccountService
     */
    protected $accountService;

    /**
     * Display a form for entering a new accounts data
     *
     * @param string $username
     */
    public function registerAction($username = null)
    {
        // Just display the form
    }

    /**
     * Create a new account and redirect to the second step (newUserAction)
     *
     * @param string $username
     * @param string $password
     * @param string $passwordRepetition
     * @throws \Neos\Flow\Mvc\Exception\ForwardException
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     * @throws \Neos\Flow\Validation\Exception\InvalidValidationConfigurationException
     * @throws \Neos\Flow\Validation\Exception\NoSuchValidatorException
     */
    public function createAccountAction($username, $password, $passwordRepetition)
    {
        // validate passwords
        if ($password !== $passwordRepetition) {
            $this->addFlashMessage(
                $this->translator->translateById('passwords.notIdentical.body'),
                $this->translator->translateById('passwords.notIdentical.title'),
                Message::SEVERITY_ERROR
            );
            $this->redirect('register', null, null, array('username' => $username));
        }

        // validate username
        $flashMessages = $this->accountService->validateUsername($username);
        if (!empty($flashMessages)) {
            foreach ($flashMessages as $flashMessage) {
                $this->addFlashMessage(...$flashMessage);
            }
            $this->redirect('register', null, null, array('username' => $username));
        }

        // create account since username and password are valid
        $account = $this->accountFactory->createAccountWithPassword($username, $password,
            array(User::USER_ROLE_CUSTOMER));
        $this->accountRepository->add($account);

        $this->forward('new', null, null, array('account' => $account));
    }

    /**
     * Display a form for entering a new users data
     *
     * @param Account $account
     */
    public function newAction(Account $account)
    {
        $this->view->assign('account', $account);
    }

    /**
     * Persist a new user
     *
     * @param User    $user
     * @param Account $account
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function createAction(User $user, Account $account)
    {
        $user->addAccount($account);
        $this->userRepository->add($user);
    }

    /**
     * Display the users profile
     *
     * @param User $user
     */
    public function showAction(User $user)
    {
        $this->view->assignMultiple(array(
                'user'    => $user,
                'account' => $user->getAccounts()->first()
            )
        );
    }

    /**
     * Display a form where the user may enter new address data
     *
     * @param User $user
     */
    public function newAddressAction(User $user)
    {
        $this->view->assign('user', $user);
    }

    /**
     * Add a new addres to the user, update the user and redirect to the show action
     *
     * @param Address $address
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function createAddressAction(Address $address)
    {
        $user = $address->getUser();
        $user->addAddress($address);
        $this->userRepository->update($user);
        $this->redirect('show', null, null, array('user' => $user));
    }

    /**
     * Display a form wehre the user may edit an address
     *
     * @param Address $address
     */
    public function editAddressAction(Address $address)
    {
        $this->view->assign('address', $address);
    }

    /**
     * Persist the changes for an address and redirect to the show action
     *
     * @param Address $address
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     * @throws \Neos\Flow\Persistence\Exception\UnknownObjectException
     */
    public function updateAddressAction(Address $address)
    {
        $this->persistenceManager->update($address);
        $this->redirect('show', null, null, array('user' => $address->getUser()));
    }

}
