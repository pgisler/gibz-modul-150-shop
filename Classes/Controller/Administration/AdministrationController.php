<?php

namespace One50\Shop\Controller\Administration;

use Neos\Error\Messages\Message;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\ActionRequest;
use Neos\Flow\Security\Account;
use Neos\Flow\Security\AccountRepository;
use Neos\Flow\Security\Authentication\Controller\AbstractAuthenticationController;
use Neos\Flow\Security\Context;
use Neos\Flow\Session\SessionInterface;
use Neos\Party\Domain\Service\PartyService;
use One50\Shop\Domain\Repository\UserRepository;
use OTPHP\Factory;
use OTPHP\TOTP;

class AdministrationController extends AbstractAuthenticationController
{

    /**
     * @Flow\Inject
     * @var PartyService
     */
    protected $partyService;

    /**
     * @Flow\Inject
     * @var Context
     */
    protected $securityContext;

    /**
     * @Flow\Inject
     * @var SessionInterface
     */
    protected $session;

    /**
     * @Flow\Inject
     * @var AccountRepository
     */
    protected $accountRepository;

    /**
     * @Flow\Inject
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * This actions shows the authentication form.
     *
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     */
    public function authAction(): void
    {
        $defaultProviderAccount = $this->securityContext->getAccountByAuthenticationProviderName('DefaultProvider');

        if ($defaultProviderAccount === null) {
            $this->redirect('index', 'Authentication');
        }

        $otpAccount = $this->accountRepository->findByAccountIdentifierAndAuthenticationProviderName(
            $defaultProviderAccount->getAccountIdentifier(),
            'TOTPProvider'
        );

        if ($otpAccount === null) {
            $this->redirect('registerOtp', null, null,
                ['accountIdentifier' => $defaultProviderAccount->getAccountIdentifier()]);
        }

        $this->view->assign('accountIdentifier', $defaultProviderAccount->getAccountIdentifier());
    }

    /**
     * @param string $accountIdentifier
     * @throws \Neos\Flow\Session\Exception\SessionNotStartedException
     */
    public function registerOtpAction(string $accountIdentifier): void
    {
        $totp = TOTP::create();
        $totp->setLabel("GIBZ Modul 150");
        $totp->setIssuer("One50");
        $totp->setParameter('accountIdentifier', $accountIdentifier);

        $provisioningUri = $totp->getProvisioningUri();
        $this->session->putData('totpProvisioningUri', [$provisioningUri]);

        $this->view->assignMultiple([
            'accountIdentifier' => $accountIdentifier,
            'provisioningUri'   => $provisioningUri,
            'qrCodeUri'         => $totp->getQrCodeUri()
        ]);
    }

    /**
     * @param string $token
     * @throws \Neos\Flow\Session\Exception\SessionNotStartedException
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function confirmOtpRegistrationAction(string $token): void
    {
        $provisioningUri = $this->session->getData('totpProvisioningUri')[0];
        $totp = Factory::loadFromProvisioningUri($provisioningUri);

        if ($totp->verify($token, null, 1)) {
            $totpAccount = new Account();
            $totpAccount->setAccountIdentifier($totp->getParameter('accountIdentifier'));
            $totpAccount->setCredentialsSource($provisioningUri);
            $totpAccount->setAuthenticationProviderName('TOTPProvider');
            $this->accountRepository->add($totpAccount);

            $user = $this->partyService->getAssignedPartyOfAccount($this->securityContext->getAccount());
            $user->addAccount($totpAccount);
            $this->userRepository->update($user);

            $interceptedRequest = $this->securityContext->getInterceptedRequest();

            if ($interceptedRequest !== null) {
                $this->redirectToRequest($interceptedRequest);
            } else {
                $this->redirect('show', 'User', null, ['user' => $user]);
            }
        } else {
            $this->addFlashMessage(
                "Die Verifizierung des Tokens ist fehlgeschlagen. Versuchen Sie bitte nochmals, den QR-Code zu scannen und anschliessend das Token zur Bestätigung zu verwenden.",
                "Ungültiges Token",
                Message::SEVERITY_ERROR
            );
            $this->forwardToReferringRequest();
        }
    }

    /**
     * @param ActionRequest|null $originalRequest
     * @return string|void
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     */
    protected function onAuthenticationSuccess(ActionRequest $originalRequest = null)
    {
        if ($originalRequest !== null) {
            $this->redirectToRequest($originalRequest);
        } else {
            $account = $this->securityContext->getAccount();
            /** @var \One50\Shop\Domain\Model\User $user */
            $user = $this->partyService->getAssignedPartyOfAccount($account);
            $this->redirect('show', 'User', null, array('user' => $user));
        }
    }

}