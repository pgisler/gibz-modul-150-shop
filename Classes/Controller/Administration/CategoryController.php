<?php

namespace One50\Shop\Controller\Administration;

use Neos\Flow\Annotations as Flow;
use One50\Shop\Controller\AbstractActionController;
use One50\Shop\Domain\Model\Category;
use Neos\Error\Messages\Message;

class CategoryController extends AbstractActionController
{

    /**
     * Translator
     *
     * @var \One50\Shop\I18n\Translator
     * @Flow\Inject
     */
    protected $translator;

    /**
     * Category repository
     *
     * @var \One50\Shop\Domain\Repository\CategoryRepository
     * @Flow\Inject
     */
    protected $categoryRepository;

    /**
     * List all available categories
     *
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('categories', $this->categoryRepository->findAll());
    }

    /**
     * Display a form for entering the new categories data
     */
    public function newAction()
    {
        // Just display the form...
    }

    /**
     * Add a new category to the repository and redirect to the index action
     *
     * @param Category $category
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function createAction(Category $category)
    {
        $this->categoryRepository->add($category);
        $this->redirect('index');
    }

    /**
     * Display a form for editing a categories properties
     *
     * @param Category $category
     */
    public function editAction(Category $category)
    {
        $this->view->assign('category', $category);
    }

    /**
     * Persist edited category properties
     *
     * @param Category $category
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function updateAction(Category $category)
    {
        $this->categoryRepository->update($category);
        $this->redirect('index');
    }

    /**
     * Remove a category from the repository
     *
     * @param Category $category
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function deleteAction(Category $category)
    {
        if ($category->getProducts()->count() === 0) {
            $this->persistenceManager->whitelistObject($category);
            $this->categoryRepository->remove($category);
        } else {
            $this->addFlashMessage(
                $this->translator->translateById('category.cannotDelete.body',
                    array($category->getTitle(), $category->getProducts()->count())),
                $this->translator->translateById('category.cannotDelete.title'),
                Message::SEVERITY_ERROR
            );
        }
        $this->redirect('index');
    }

}
