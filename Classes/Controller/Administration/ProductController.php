<?php

namespace One50\Shop\Controller\Administration;

/*
 * This file is part of the One50.Shop package.
 */

use Neos\Flow\Annotations as Flow;
use One50\Shop\Controller\AbstractActionController;
use One50\Shop\Domain\Model\Product;
use Neos\Error\Messages\Message;

class ProductController extends AbstractActionController
{

    /**
     * @var \One50\Shop\I18n\Translator
     * @Flow\Inject
     */
    protected $translator;

    /**
     * @var \One50\Shop\Domain\Repository\CategoryRepository
     * @Flow\Inject
     */
    protected $categoryRepository;

    /**
     * @var \One50\Shop\Domain\Repository\ProductRepository
     * @Flow\Inject
     */
    protected $productRepository;

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('products', $this->productRepository->findAll());
    }

    /**
     * Display a form for entering the new products data
     */
    public function newAction()
    {
        //@TODO: Prevent product creation when no category exists
        $this->view->assign('categories', $this->categoryRepository->findAll());
    }

    /**
     * Persist a new product and redirect to the index action
     *
     * @param Product $product
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function createAction(Product $product)
    {
        $this->productRepository->add($product);
        $this->redirect('index');
    }

    /**
     * Display a form for editing the products properties
     *
     * @param Product $product
     */
    public function editAction(Product $product)
    {
        $this->view->assignMultiple(array(
                'categories' => $this->categoryRepository->findAll(),
                'product'    => $product
            )
        );
    }

    /**
     * Persist edited product properties and redirect to the index action
     *
     * @param Product $product
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function updateAction(Product $product)
    {
        $this->productRepository->update($product);
        $this->redirect('index');
    }

    /**
     * Display a flash message why products cannot be deleted right now
     *
     * @throws \Neos\Flow\Mvc\Exception\StopActionException
     */
    public function deleteAction()
    {
        $this->addFlashMessage(
            $this->translator->translateById('product.cannotDelete.body'),
            $this->translator->translateById('product.cannotDelete.title'),
            Message::SEVERITY_ERROR
        );
        $this->redirect('index');
    }

}
