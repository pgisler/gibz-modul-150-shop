<?php
namespace One50\Shop\Controller;

/*
 * This file is part of the One50.Shop package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Security\Account;

abstract class AbstractActionController extends \Neos\Flow\Mvc\Controller\ActionController {
	
	/**
	 * Shopping Cart
	 *
	 * @var \One50\Shop\Service\Cart
	 * @Flow\Inject
	 */
	protected $cart;
	
	/**
	 * Security Context
	 *
	 * @var \Neos\Flow\Security\Context
	 * @Flow\Inject
	 */
	protected $securityContext;
	
	/**
	 * Party Service
	 *
	 * @var \Neos\Party\Domain\Service\PartyService
	 * @Flow\Inject
	 */
	protected $partyService;
	
	/**
	 * Category Repository
	 *
	 * @var \One50\Shop\Domain\Repository\CategoryRepository
	 * @Flow\Inject
	 */
	protected $categoryRepository;
	
	/**
	 * User
	 *
	 * @var \One50\Shop\Domain\Model\User
	 */
	protected $user;
	
	/**
	 * Assign the users cart items to the view
	 */
	protected function initializeView(\Neos\Flow\Mvc\View\ViewInterface $view) {
		// assign categories to view
		$this->view->assign('categories', $this->categoryRepository->findAll());
		
		// assign cart to view
		$view->assign('cart', $this->cart);
		
		// assign user to view (if available)
		$account = $this->securityContext->getAccount();
		if ($account instanceof Account) {
			$this->user = $this->partyService->getAssignedPartyOfAccount($account);
			$this->view->assign('user', $this->user);
		}

		$this->view->assign('tokens', $this->securityContext->getAuthenticationTokens());
	}
}
