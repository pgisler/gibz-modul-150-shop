<?php

namespace One50\Shop\Security\Authentication\Token;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\ActionRequest;
use Neos\Flow\Security\Authentication\Token\AbstractToken;
use Neos\Utility\ObjectAccess;

class TOTPToken extends AbstractToken
{

    /**
     * @Flow\Transient
     * @var array
     */
    protected $credentials = ['accountIdentifier' => '', 'totp' => ''];

    /**
     * @param ActionRequest $actionRequest
     * @return bool|void
     * @throws \Neos\Flow\Security\Exception\InvalidAuthenticationStatusException
     */
    public function updateCredentials(ActionRequest $actionRequest)
    {
        $httpRequest = $actionRequest->getHttpRequest();
        if ($httpRequest->getMethod() !== 'POST') {
            return;
        }

        $postArguments = $actionRequest->getInternalArguments();

        $totp = ObjectAccess::getPropertyPath($postArguments,
            '__authentication.One50.Shop.Security.Authentication.Token.TOTPToken.totp');

        $accountIdentifier = ObjectAccess::getPropertyPath($postArguments,
            '__authentication.One50.Shop.Security.Authentication.Token.TOTPToken.accountIdentifier');

        if (!empty($totp) && !empty($accountIdentifier)) {
            $this->credentials['accountIdentifier'] = $accountIdentifier;
            $this->credentials['totp'] = $totp;
            $this->setAuthenticationStatus(self::AUTHENTICATION_NEEDED);
        }
    }

    /**
     * Returns a string representation for logging purposes.
     *
     * @return string
     */
    public function __toString()
    {
        return "TOTP: {$this->credentials['totp']}";
    }


}