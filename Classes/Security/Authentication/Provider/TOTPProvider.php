<?php

namespace One50\Shop\Security\Authentication\Provider;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\PersistenceManagerInterface;
use Neos\Flow\Security\Account;
use Neos\Flow\Security\AccountRepository;
use Neos\Flow\Security\Authentication\Provider\AbstractProvider;
use Neos\Flow\Security\Authentication\TokenInterface;
use Neos\Flow\Security\Context;
use Neos\Flow\Security\Exception\UnsupportedAuthenticationTokenException;
use One50\Shop\Security\Authentication\Token\TOTPToken;
use OTPHP\Factory;
use OTPHP\TOTP;

/**
 *
 */
class TOTPProvider extends AbstractProvider
{

    /**
     * @Flow\Inject
     * @var PersistenceManagerInterface
     */
    protected $persistenceManager;

    /**
     * @Flow\Inject
     * @var Context
     */
    protected $securityContext;

    /**
     * @Flow\Inject
     * @var AccountRepository
     */
    protected $accountRepository;

    public function getTokenClassNames()
    {
        return [TOTPToken::class];
    }

    /**
     * @param TokenInterface $authenticationToken
     * @throws UnsupportedAuthenticationTokenException
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     * @throws \Neos\Flow\Security\Exception\InvalidAuthenticationStatusException
     * @throws \Exception
     */
    public function authenticate(TokenInterface $authenticationToken)
    {
        if (!($authenticationToken instanceof TOTPToken)) {
            throw new UnsupportedAuthenticationTokenException('This provider cannot authenticate the given token.',
                1217339840);
        }

        /** @var Account $account */
        $account = null;
        $credentials = $authenticationToken->getCredentials();

        if ($authenticationToken->getAuthenticationStatus() !== TokenInterface::AUTHENTICATION_SUCCESSFUL) {
            $authenticationToken->setAuthenticationStatus(TokenInterface::NO_CREDENTIALS_GIVEN);
        }

        if (!is_array($credentials) || !isset($credentials['totp']) || !isset($credentials['accountIdentifier'])) {
            return;
        }

        $providerName = $this->name;
        $accountRepository = $this->accountRepository;
        $this->securityContext->withoutAuthorizationChecks(function () use (
            $credentials,
            $providerName,
            $accountRepository,
            &$account
        ) {
            $account = $accountRepository->findActiveByAccountIdentifierAndAuthenticationProviderName($credentials['accountIdentifier'],
                $providerName);
        });

        $authenticationToken->setAuthenticationStatus(TokenInterface::WRONG_CREDENTIALS);

        if ($account === null) {
            return;
        }

        $totpProvisioningUrl = $account->getCredentialsSource();
        try {
            $totp = Factory::loadFromProvisioningUri($totpProvisioningUrl);
        } catch (\InvalidArgumentException $exception) {
            // Create dummy TOTP to prevent timing attacks on this provider
            $totp = TOTP::create();
        }

        if ($totp->verify($credentials['totp'])) {
            $account->authenticationAttempted(TokenInterface::AUTHENTICATION_SUCCESSFUL);
            $authenticationToken->setAuthenticationStatus(TokenInterface::AUTHENTICATION_SUCCESSFUL);
            $authenticationToken->setAccount($account);
        } else {
            $account->authenticationAttempted(TokenInterface::WRONG_CREDENTIALS);
        }

        $this->accountRepository->update($account);
        $this->persistenceManager->whitelistObject($account);
    }

}