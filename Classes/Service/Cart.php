<?php
namespace One50\Shop\Service;

use Doctrine\Common\Collections\ArrayCollection;
use One50\Shop\Domain\Model\CartItem;
use One50\Shop\Domain\Model\Order;
use One50\Shop\Domain\Model\OrderItem;
use One50\Shop\Domain\Model\Product;
use Neos\Flow\Annotations as Flow;

/**
 * Class Cart
 *
 * @package One50\Shop\Service
 * @Flow\Scope("session")
 */
class Cart {
	
	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection<\One50\Shop\Domain\Model\CartItem>
	 */
	protected $items;
	
	/**
	 * @param CartItem $newItem
	 * @Flow\Session(autoStart = TRUE)
	 */
	public function addItem(CartItem $newItem) {
		if (is_null($this->items)) {
			$this->items = new ArrayCollection();
		}
		
		$inCart = false;
		/** @var CartItem $item */
		foreach ($this->getItems() as $item) {
			if ($item->getProduct() == $newItem->getProduct()) {
				$inCart = true;
				$item->setQuantity($item->getQuantity() + $newItem->getQuantity());
			}
		}
		
		if (!$inCart) {
			$this->items->add($newItem);
		}
	}
	
	/**
	 * Remove an item from the cart
	 *
	 * @param Product $product
	 */
	public function removeItemByProduct(Product $product) {
		$items = $this->getItems();
		/** @var CartItem $item */
		foreach ($items as $item) {
			var_dump($item->getProduct()->getTitle());
			if ($item->getProduct() == $product) {
				$items->removeElement($item);
				break;
			}
		}
		$this->setItems($items);
	}
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getItems() {
		return $this->items;
	}
	
	/**
	 * @param ArrayCollection $items
	 */
	public function setItems(ArrayCollection $items) {
		$this->items = $items;
	}
	
	/**
	 * Empty the cart
	 */
	public function clear() {
		$this->items = new ArrayCollection();
	}
	
	/**
	 * Convert the cart to an order object by converting the cart items to order items and appending them to a new
	 * order object
	 *
	 * @return Order
	 */
	public function convertToOrder() {
		$order = new Order();
		$order->setStatus(Order::ORDER_STATUS_PENDING);
		$order->setTransactionId(uniqid());
		$order->setOrderDate(new \DateTime());
		
		$orderItems = new ArrayCollection();
		
		/** @var CartItem $cartItem */
		foreach ($this->getItems() as $cartItem) {
			$orderItem = new OrderItem();
			$orderItem->setParentOrder($order);
			$orderItem->setProduct($cartItem->getProduct());
			$orderItem->setQuantity($cartItem->getQuantity());
			$orderItems->add($orderItem);
		}
		$order->setItems($orderItems);
		
		return $order;
	}
	
}